import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

import Hero from './components/Hero'

function App() {
  const [form, setForm] = useState({
    form: {
      rtf: 0,
      eps: 'sura'
    }
  })

  const onChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.targe.value,
    })
  }

  return (
    <div className="App">
      <Hero />
    </div>
  );
}

export default App;
