import styled from '@emotion/styled'

import { Color } from './layout/theme'

export const BannerPart = styled.section`
  background-color: ${Color.gallery};
  height: 100vh;
  overflow: hidden;
`
export const BannerText = styled.div`
  display: table;
  width: 100%;
  height: 100vh;
  padding-top: 40px;
  text-align: left;

  @media (max-width: 575px) {
    text-align: center;
    padding-top: 0px;
    height: 700px;
  }
`
export const BannerTextInner = styled.div`
  display: table-cell;
  vertical-align: middle;

  @media (max-width: 575px) {
    text-align: center;
    padding-top: 0px;
    height: 700px;
  }
`
export const Hey = styled.h5`
  color: ${Color.moodyBlue};;
  font-size: 25px;
  font-family: "Poppins", sans-serif;
  margin-bottom: -10px;
`
export const Title = styled.h1`
  font-family: "Anton", sans-serif;
  font-size: 100px;
  margin-bottom: 30px;
  line-height: 1.273;
  color: ${Color.codGray};;
  text-transform: uppercase;
  position: relative;
  z-index: 1;

  &::after {
    position: absolute;
    content: "";
    left: 3px;
    width: 99%;
    height: 25px;
    background-color: ${Color.moodyBlue};;
    bottom: 21px;
    z-index: -1;
  }

  @media (max-width: 991px) {
    font-size: 40px;
    margin-bottom: 15px;
    line-height: 1.3;
    display: inline-block;

    &::after {
      position: absolute;
      content: "";
      left: 0;
      width: 100%;
      height: 10px;
      background-color: ${Color.moodyBlue};;
      bottom: 8px;
      z-index: -1;
    }
  }
`
export const Description = styled.p`
  margin-bottom: 58px;
  line-height: 1.8;
  color: ${Color.codGray};;
  font-size: 15px;
`
export const Button = styled.button`
  background-image: linear-gradient(to right, ${Color.purpleHeart} 0%, ${Color.governorBay} 50%, ${Color.purpleHeart} 100%);
  padding: 18px 70px;
  display: inline-block;
  font-size: 15px;
  color: ${Color.white};;
  transition: 0.5s;
  text-transform: uppercase;
  border: 1px solid transparent;
  letter-spacing: 1.5px;
  background-size: 200% auto;
  color: ${Color.white} !important;
`
